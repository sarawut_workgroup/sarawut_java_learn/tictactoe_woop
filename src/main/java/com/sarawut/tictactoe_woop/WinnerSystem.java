/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sarawut.tictactoe_woop;

/**
 *
 * @author max-s
 */
public class WinnerSystem {

    public boolean checkFinish(char table[][], char turn, int round) {
        char result = checkWinner(table, turn, round);
        if (result == '-') {
            return false;
        } else {
            return true;
        }
    }

    /*
     * Return 'X' = X win
     * Return 'O' = O win
     * Return '0' = no one win
     * Return '-' = game in progress
     */
    public char checkWinner(char table[][], char turn, int round) {
        char result = checkHoVe(table, turn);
        if (result == '-') {
            result = checkDiagonal(table, turn);
        }
        if (round == 9) {
            return '0';
        }
        return result;
    }

    //hove = Horizon and Vertical
    private char checkHoVe(char table[][], char turn) {
        for (int i = 0; i < 3; i++) {
            if (table[i][0] == turn && table[i][1] == turn
                    && table[i][2] == turn) {
                return turn;
            } else if (table[0][i] == turn && table[1][i] == turn
                    && table[2][i] == turn) {
                return turn;
            }
        }
        return '-';
    }

    private char checkDiagonal(char table[][], char turn) {
        if (table[0][0] == turn && table[1][1] == turn && table[2][2] == turn) {
            return turn;
        } else if (table[2][0] == turn && table[1][1] == turn
                && table[0][2] == turn) {
            return turn;
        } else {
            return '-';
        }
    }

    protected void showWinner(char winner) {
        if (winner == '0') {
            System.out.println("No player win....");
        } else {
            System.out.println("Player " + winner + " win....");
        }
        System.out.println("Bye bye ....");
    }
}
