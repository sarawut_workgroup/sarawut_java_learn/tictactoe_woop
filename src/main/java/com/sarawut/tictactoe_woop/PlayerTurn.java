/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sarawut.tictactoe_woop;

import java.util.Random;

/**
 *
 * @author max-s
 */
public class PlayerTurn {

    private char currentTurn;

    protected PlayerTurn() {
        Random rnd = new Random();
        if ((rnd.nextInt(10) % 2) == 0) {
            currentTurn = 'O';
        } else {
            currentTurn = 'X';
        }
    }

    protected char getTurn() {
        return currentTurn;
    }

    protected void changeTurn() {
        if (currentTurn == 'X') {
            currentTurn = 'O';
        } else if (currentTurn == 'O') {
            currentTurn = 'X';
        }
    }
}
