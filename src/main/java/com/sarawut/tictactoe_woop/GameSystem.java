/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sarawut.tictactoe_woop;

import java.util.Scanner;

/**
 *
 * @author max-s
 */
public class GameSystem {

    private final Table gameTable = new Table();
    private final PlayerTurn turn = new PlayerTurn();
    private final ExceptionInput check = new ExceptionInput();
    private final WinnerSystem winner = new WinnerSystem();

    protected void runGame() {
        Scanner input = new Scanner(System.in);
        showWelcome();
        for (int i = 1; i <= 9;) {
            int j = 0;
            showTurn();
            j = inputPosition(input.next(), input.next());
            if (winnerCheck(i) != 1) {
                break;
            } else {
                if (j == 1) {
                    showTable();
                    changeTurn();
                    i += j;
                }
            }
        }
        input.close();
    }

    private void showWelcome() {
        System.out.println("Welcome to OX Game");
        showTable();
    }

    private void showTable() {
        gameTable.getTable();
    }

    private void showTurn() {
        System.out.println(turn.getTurn() + " turn");
        System.out.println("Please input Row Col: ");
    }

    private void changeTurn() {
        turn.changeTurn();
    }

    /*
     * Return 1 = no error
     * Return 0 = error input incorrect type, position is out of range, 
                    position is full
     */
    private int inputPosition(String yPos, String xPos) {
        if (check.checkMismatch(yPos, xPos)) {
            int yPosID = Integer.parseInt(yPos) - 1;
            int xPosID = Integer.parseInt(xPos) - 1;
            if (check.checkPosition(yPosID, xPosID, gameTable.getArrTable())) {
                gameTable.inputPosition(yPosID, xPosID, turn.getTurn());
                return 1;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    private int winnerCheck(int round) {
        char wincode = winner.checkWinner(gameTable.getArrTable(),
                turn.getTurn(), round);
        if (!winner.checkFinish(gameTable.getArrTable(),
                turn.getTurn(), round)) {
            return 1;
        } else {
            winner.showWinner(wincode);
            return 9;
        }
    }

}
