/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sarawut.tictactoe_woop;

/**
 *
 * @author max-s
 */
public class Table {

    private char[][] table = {{'-', '-', '-'},
    {'-', '-', '-'},
    {'-', '-', '-'}};

    protected void getTable() {
        for (int i = 0; i < table[0].length; i++) {
            System.out.print(" " + (i + 1));
        }
        System.out.println("");
        for (int i = 0; i < table.length; i++) {
            System.out.print(i + 1);
            for (int j = 0; j < table[i].length; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
    }

    public char[][] getArrTable() {
        return table;
    }

    //parameter is index
    public void inputPosition(int x, int y, char playerTurn) {
        table[x][y] = playerTurn;
    }
}
