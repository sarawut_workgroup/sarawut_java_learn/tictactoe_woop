/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sarawut.tictactoe_woop;

/**
 *
 * @author max-s
 */
public class ExceptionInput {

    protected boolean checkMismatch(String posXInput, String posYInput) {
        try {
            Integer.parseInt(posXInput);
            Integer.parseInt(posYInput);
        } catch (NumberFormatException error) {
            System.out.println("Input mismatch! Please input again!");
            return false;
        }
        return true;
    }

    protected boolean checkPosition(int y, int x, char[][] table) {
        if (checkRange(y, x, table) && checkEmpty(y, x, table)) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkRange(int y, int x, char[][] table) {
        if (((y + 1) > 0) && ((x + 1) > 0)) {
            if ((table.length >= (y + 1)) && (table[0].length >= (x + 1))) {
                return true;
            } else {
                System.out.println("Your is out of table, Please input Row "
                        + "Col (not above 3 and less than 1)!: ");
                return false;
            }
        } else {
            System.out.println("Your is out of table, Please input Row "
                    + "Col (not above 3 and less than 1)!: ");
            return false;
        }
    }

    private boolean checkEmpty(int y, int x, char[][] table) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if ((i == y && j == x) && (table[i][j] == '-')) {
                    return true;
                } else if ((i == y && j == x) && (table[i][j] != '-')){
                    System.out.println("Your Row Col is not empty, "
                            + "Please input again!: ");
                    return false;
                }
            }
        }
        return true;
    }
}
