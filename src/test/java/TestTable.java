/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.sarawut.tictactoe_woop.Table;
import com.sarawut.tictactoe_woop.WinnerSystem;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author max-s
 */
public class TestTable {

    public TestTable() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testRow1ByX() {
        int row = 0;
        char player = 'X';
        Table table = new Table();
        WinnerSystem win = new WinnerSystem();
        table.inputPosition(row, 0, player);
        table.inputPosition(row, 1, player);
        table.inputPosition(row, 2, player);

        assertEquals(true, win.checkFinish(table.getArrTable(), player, 3));
        assertEquals(player, win.checkWinner(table.getArrTable(), player, 3));
    }

    @Test
    public void testRow2ByX() {
        int row = 1;
        char player = 'X';
        Table table = new Table();
        WinnerSystem win = new WinnerSystem();
        table.inputPosition(row, 0, player);
        table.inputPosition(row, 1, player);
        table.inputPosition(row, 2, player);

        assertEquals(true, win.checkFinish(table.getArrTable(), player, 3));
        assertEquals(player, win.checkWinner(table.getArrTable(), player, 3));
    }

    @Test
    public void testRow3ByX() {
        int row = 2;
        char player = 'X';
        Table table = new Table();
        WinnerSystem win = new WinnerSystem();
        table.inputPosition(row, 0, player);
        table.inputPosition(row, 1, player);
        table.inputPosition(row, 2, player);

        assertEquals(true, win.checkFinish(table.getArrTable(), player, 3));
        assertEquals(player, win.checkWinner(table.getArrTable(), player, 3));
    }

    @Test
    public void testCol1ByX() {
        int col = 0;
        char player = 'X';
        Table table = new Table();
        WinnerSystem win = new WinnerSystem();
        table.inputPosition(0, col, player);
        table.inputPosition(1, col, player);
        table.inputPosition(2, col, player);

        assertEquals(true, win.checkFinish(table.getArrTable(), player, 3));
        assertEquals(player, win.checkWinner(table.getArrTable(), player, 3));
    }

    @Test
    public void testCol2ByX() {
        int col = 1;
        char player = 'X';
        Table table = new Table();
        WinnerSystem win = new WinnerSystem();
        table.inputPosition(0, col, player);
        table.inputPosition(1, col, player);
        table.inputPosition(2, col, player);

        assertEquals(true, win.checkFinish(table.getArrTable(), player, 3));
        assertEquals(player, win.checkWinner(table.getArrTable(), player, 3));
    }

    @Test
    public void testCol3ByX() {
        int col = 2;
        char player = 'X';
        Table table = new Table();
        WinnerSystem win = new WinnerSystem();
        table.inputPosition(0, col, player);
        table.inputPosition(1, col, player);
        table.inputPosition(2, col, player);

        assertEquals(true, win.checkFinish(table.getArrTable(), player, 3));
        assertEquals(player, win.checkWinner(table.getArrTable(), player, 3));
    }

    @Test
    public void testDiagonalLtoRByX() {
        char player = 'X';
        Table table = new Table();
        WinnerSystem win = new WinnerSystem();
        table.inputPosition(0, 0, player);
        table.inputPosition(1, 1, player);
        table.inputPosition(2, 2, player);

        assertEquals(true, win.checkFinish(table.getArrTable(), player, 3));
        assertEquals(player, win.checkWinner(table.getArrTable(), player, 3));
    }

    @Test
    public void testDiagonalRtoLByX() {
        char player = 'X';
        Table table = new Table();
        WinnerSystem win = new WinnerSystem();
        table.inputPosition(0, 2, player);
        table.inputPosition(1, 1, player);
        table.inputPosition(2, 0, player);

        assertEquals(true, win.checkFinish(table.getArrTable(), player, 3));
        assertEquals(player, win.checkWinner(table.getArrTable(), player, 3));
    }

    @Test
    public void testRow1ByO() {
        int row = 0;
        char player = 'O';
        Table table = new Table();
        WinnerSystem win = new WinnerSystem();
        table.inputPosition(row, 0, player);
        table.inputPosition(row, 1, player);
        table.inputPosition(row, 2, player);

        assertEquals(true, win.checkFinish(table.getArrTable(), player, 3));
        assertEquals(player, win.checkWinner(table.getArrTable(), player, 3));
    }

    @Test
    public void testRow2ByO() {
        int row = 1;
        char player = 'O';
        Table table = new Table();
        WinnerSystem win = new WinnerSystem();
        table.inputPosition(row, 0, player);
        table.inputPosition(row, 1, player);
        table.inputPosition(row, 2, player);

        assertEquals(true, win.checkFinish(table.getArrTable(), player, 3));
        assertEquals(player, win.checkWinner(table.getArrTable(), player, 3));
    }

    @Test
    public void testRow3ByO() {
        int row = 2;
        char player = 'O';
        Table table = new Table();
        WinnerSystem win = new WinnerSystem();
        table.inputPosition(row, 0, player);
        table.inputPosition(row, 1, player);
        table.inputPosition(row, 2, player);

        assertEquals(true, win.checkFinish(table.getArrTable(), player, 3));
        assertEquals(player, win.checkWinner(table.getArrTable(), player, 3));
    }

    @Test
    public void testCol1ByO() {
        int col = 0;
        char player = 'O';
        Table table = new Table();
        WinnerSystem win = new WinnerSystem();
        table.inputPosition(0, col, player);
        table.inputPosition(1, col, player);
        table.inputPosition(2, col, player);

        assertEquals(true, win.checkFinish(table.getArrTable(), player, 3));
        assertEquals(player, win.checkWinner(table.getArrTable(), player, 3));
    }

    @Test
    public void testCol2ByO() {
        int col = 1;
        char player = 'O';
        Table table = new Table();
        WinnerSystem win = new WinnerSystem();
        table.inputPosition(0, col, player);
        table.inputPosition(1, col, player);
        table.inputPosition(2, col, player);

        assertEquals(true, win.checkFinish(table.getArrTable(), player, 3));
        assertEquals(player, win.checkWinner(table.getArrTable(), player, 3));
    }

    @Test
    public void testCol3ByO() {
        int col = 2;
        char player = 'O';
        Table table = new Table();
        WinnerSystem win = new WinnerSystem();
        table.inputPosition(0, col, player);
        table.inputPosition(1, col, player);
        table.inputPosition(2, col, player);

        assertEquals(true, win.checkFinish(table.getArrTable(), player, 3));
        assertEquals(player, win.checkWinner(table.getArrTable(), player, 3));
    }

    @Test
    public void testDiagonalLtoRByO() {
        char player = 'O';
        Table table = new Table();
        WinnerSystem win = new WinnerSystem();
        table.inputPosition(0, 0, player);
        table.inputPosition(1, 1, player);
        table.inputPosition(2, 2, player);

        assertEquals(true, win.checkFinish(table.getArrTable(), player, 3));
        assertEquals(player, win.checkWinner(table.getArrTable(), player, 3));
    }

    @Test
    public void testDiagonalRtoLByO() {
        char player = 'O';
        Table table = new Table();
        WinnerSystem win = new WinnerSystem();
        table.inputPosition(0, 2, player);
        table.inputPosition(1, 1, player);
        table.inputPosition(2, 0, player);

        assertEquals(true, win.checkFinish(table.getArrTable(), player, 3));
        assertEquals(player, win.checkWinner(table.getArrTable(), player, 3));
    }

    @Test
    public void testFullnFinish() {
        Table table = new Table();
        WinnerSystem win = new WinnerSystem();
        char player = 'O';
        table.inputPosition(0, 2, player);
        table.inputPosition(1, 0, player);
        table.inputPosition(1, 1, player);
        table.inputPosition(2, 2, player);
        player = 'X';
        table.inputPosition(0, 0, player);
        table.inputPosition(0, 1, player);
        table.inputPosition(1, 2, player);
        table.inputPosition(2, 0, player);
        table.inputPosition(2, 1, player);

        assertEquals(true, win.checkFinish(table.getArrTable(), player, 9));
        assertEquals('0', win.checkWinner(table.getArrTable(), player, 9));
    }

    @Test
    public void testNotFullnNotFinish() {
        Table table = new Table();
        WinnerSystem win = new WinnerSystem();
        char player = 'O';
        table.inputPosition(0, 2, player);
        table.inputPosition(1, 0, player);
        table.inputPosition(1, 1, player);
        player = 'X';
        table.inputPosition(0, 0, player);
        table.inputPosition(0, 1, player);
        table.inputPosition(2, 0, player);
        table.inputPosition(2, 1, player);

        assertEquals(false, win.checkFinish(table.getArrTable(), player, 3));
        assertEquals('-', win.checkWinner(table.getArrTable(), player, 3));
    }
}
